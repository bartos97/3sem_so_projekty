#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <wait.h>

//DEKLARACJE
//-------------------------------

//@brief tworzy podana ilosc procesow producer.exe
//@param howMany - ile procesow stworzyc
void createProducers(int howMany);

//@brief tworzy podana ilosc procesow consumer.exe
//@param howMany - ile procesow stworzyc
void createConsumers(int howMany);

//@brief tworzy potok nazwany fifo_buffer w obecnym katalogu
void createFIFO(void);

static const char* fifoPath = "/tmp/Z2";


//START
//-------------------------------
int main(int argc, char* argv[]) {

    //Kontrola argumentow wywolania
    if (argc != 3)  {
        printf("Niepoprawna ilosc argumentow programu. Wymagane 2: ilosc producentow, ilosc konsumentow\n");
        exit(EXIT_FAILURE);
    }

//    char* envMaxProc = getenv("MAXPROC");
//    int maxProc = 0;

//    if (envMaxProc != NULL) {
//        maxProc = atoi(getenv("MAXPROC"));
//    }
//    else {
//        printf("Nie ustawiono zmiennej srodowiskowej MAXPROC\n");
//        exit(EXIT_FAILURE);
//    }

    int numOfProducers = atoi(argv[1]);
    int numOfConsumers = atoi(argv[2]);
    if (numOfProducers < 0 || numOfConsumers < 0) {
        printf("Niepoprawne dane wejsciowe\n");
        exit(EXIT_FAILURE);
    }

//    if (numOfConsumers + numOfProducers > maxProc) {
//        printf("Podano zbyt duza ilosc programow potomnych do utworzenia. Max: %d\n", maxProc);
//        exit(EXIT_FAILURE);
//    }
    if (numOfConsumers > 256 || numOfProducers > 256) {
        printf("Podano zbyt duza ilosc programow potomnych do utworzenia.");
        exit(EXIT_FAILURE);
    }

    //czesc wlasciwa programu
    if (numOfProducers != 0) {
        createFIFO();
    }
    createProducers(numOfProducers);
    createConsumers(numOfConsumers);
  
    //zamykanie potomnych
    int i;
    for(i = 0; i < numOfProducers + numOfConsumers; i++) {
        int waitStatus;
        pid_t waitID = wait(&waitStatus);
        if (waitID == -1){
            perror("Blad wait()");
            exit(EXIT_FAILURE);
        }
        printf("\tMAIN: Zakonczyl sie potomny %d ze statusem %d\n", waitID, waitStatus);
    }

    if (numOfProducers != 0) {
        if (unlink(fifoPath) == -1) {
            perror("main -> unlink error");
            exit(EXIT_FAILURE);
        }
    }
    return 0;
}


//DEFINICJE FUNKCJI
//------------------------------
void createFIFO(void) {
    if (mkfifo(fifoPath, 0660) == -1) {
        if (errno == EEXIST) {
            unlink(fifoPath);
            createFIFO();
            return;
        }
        perror("main -> mkfifo error");
    }
    else {
        printf("\tMAIN: stworzono FIFO\n");
    }
}


void createProducers(int howMany) {
    int i;
    for(i = 0; i < howMany; i++) {
        switch(fork()) {

        //fork error
        case -1:
            perror("main -> createProducers -> fork error");
            exit(EXIT_FAILURE);

        //child process
        case 0:
            if (execl("./producer.exe", "producer.exe", NULL) == -1) {
                perror("main -> createProducers -> execl error");
                exit(EXIT_FAILURE);
            }
            //exit(EXIT_SUCCESS);

        //parent process
        default:
            break;
        }
    }
}


void createConsumers(int howMany) {
    int i;
    for(i = 0; i < howMany; i++) {
        switch(fork()) {

        //fork error
        case -1:
            perror("main -> createConsumers -> fork error");
            exit(EXIT_FAILURE);

        //child process
        case 0:
            if (execl("./consumer.exe", "consumer.exe", NULL) == -1) {
                perror("main -> createConsumers -> execl error");
                exit(EXIT_FAILURE);
            }
            //exit(EXIT_SUCCESS);

        //parent process
        default:
            break;
        }
    }
}
