#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>

//DEKLARACJE
//-------------------------------

//@brief zapisuje dane do wskazanego FIFO
//@param fifoPath - sciezka do pliku FIFO
void produceData(const char* fifoPath);

//@brief generuje w katalogu ./input/ plik o nazwie PID wywolujacego procesu
//wypelniony losowa iloscia znakow (do 20 max) o losowych wartosciach ASCII od 32 do 126
//@param filePath - tablica w ktorej zostanie zapisana sciezka do nowo utworzonego pliku
void generateFile(char* filePath);


//START
//-------------------------------
int main() {
    srand(getpid());
    produceData("/tmp/Z2");
    return 0;
}


//DEFINICJE FUNKCJI
//------------------------------
void produceData(const char* fifoPath) {
    pid_t thisProc = getpid();

    char inputFilePath[32];
    generateFile(inputFilePath);

    FILE* inputFile;
    inputFile = fopen(inputFilePath, "r");
    if (inputFile == NULL) {
        perror("producer -> produceData -> fopen error");
        exit(EXIT_FAILURE);
    }

    //otwarcie FIFO
    int fifoID;
    while (1) {
        fifoID = open(fifoPath, O_WRONLY);
        if (fifoID == -1) {
            if (errno == ENXIO) {
                printf("PRODUCENT %d: czekam na otwarcie FIFO do czytania\n", thisProc);
                continue;
            }
            perror("producer -> produceData -> open() error");
            exit(EXIT_FAILURE);
        }
        else break;
    }

    printf("PRODUCENT %d: start produkcji\n", thisProc);

    char singleChar;
    ssize_t bytesWriten;
    while ((singleChar = getc(inputFile)) != EOF) {
        while (1) {
            bytesWriten = write(fifoID, &singleChar, sizeof(char));
            if (bytesWriten == -1) {
                if (errno == EPIPE) {
                    printf("PRODUCENT %d: czekam na otwarcie FIFO do czytania\n", thisProc);
                    continue;
                }
                perror("producer -> produceData -> write() error");
                exit(EXIT_FAILURE);
            }
            printf("-->PRODUCENT %d: zapisalem %ldB danych: %c\n", thisProc, bytesWriten, singleChar);
            break;
        }
    }

    printf("PRODUCENT %d: koniec produkcji\n", thisProc);
    fclose(inputFile);

    //zamykniecie FIFO
    int returnCode = close(fifoID);
    if (returnCode == -1) {
        perror("producer -> produceData -> close() error");
        exit(EXIT_FAILURE);
    }
}


void generateFile(char* filePath) {
  FILE* file;
  int numberOfChars = rand() % 19 + 1;

  sprintf(filePath, "./input/%d.data", getpid());

  file = fopen(filePath, "w");
  if (file == NULL) {
      perror("generateFile fopen error");
      exit(EXIT_FAILURE);
  }

  int i;
  char printableAscii;
  for(i = 0; i < numberOfChars; i++) {
    printableAscii = rand() % (126-32) + 32;
    fprintf(file, "%c", printableAscii);
  }

  fclose(file);
}
