#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

//DEKLARACJE
//-------------------------------

//@brief odczytuje dane ze wskazanego FIFO
//@param fifoPath - sciezka do pliku FIFO
void consumeData(const char* fifoPath);


//START
//-------------------------------
int main() {
    consumeData("/tmp/Z2");
    return 0;
}


//DEFINICJE FUNKCJI
//------------------------------
void consumeData(const char* fifoPath) {
    pid_t thisProc = getpid();

    char outputFilePath[32];
    sprintf(outputFilePath, "output/%d.data", getpid());

    FILE* outputFile;
    outputFile = fopen(outputFilePath, "w");
    if (outputFile == NULL) {
        perror("consumer -> consumeData -> fopen error");
        exit(EXIT_FAILURE);
    }

    //otwarcie FIFO
    int fifoID;
    fifoID = open(fifoPath, O_RDONLY);
    if (fifoID == -1) {
        perror("consumer -> consumeData -> open() error");
        exit(EXIT_FAILURE);
    }

    char singleChar;
    ssize_t bytesRead;
    printf("KONSUMENT %d: start konsumpcji\n", thisProc);

    while(1) {
        bytesRead = read(fifoID, &singleChar, sizeof(char));
        if (bytesRead == -1) {
            perror("consumer -> consumeData -> read() error");
            exit(EXIT_FAILURE);
        }
        else if (bytesRead == 0 || singleChar == EOF)
            break;
        else {
            printf("<--KONSUMENT %d: pobralem %ldB danych: %c\n", thisProc, bytesRead, singleChar);
            putc(singleChar, outputFile);
        }
    }

    printf("KONSUMENT %d: koniec konsumpcji\n", thisProc);
    fclose(outputFile);

    //zamykniecie FIFO
    int returnCode = close(fifoID);
    if (returnCode == -1) {
      perror("consumer -> consumeData -> close() error");
      exit(EXIT_FAILURE);
    }
}
