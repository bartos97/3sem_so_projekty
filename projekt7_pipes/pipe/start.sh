#!/bin/bash

PROCLIMIT="$(ulimit -u)"
PIPELIMIT="$(ulimit -p)"
ACTIVEPROC="$(ps aux | wc -l)"

MAXPROC=$((PROCLIMIT-ACTIVEPROC-512))
MAXPIPE=$((PIPELIMIT*512))

export MAXPROC
export MAXPIPE

mkdir input 2>/dev/null
mkdir output 2>/dev/null

./cleaner.sh
make
./main.exe $1 $2
./show_result.sh
