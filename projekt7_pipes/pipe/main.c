#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <wait.h>

//DEKLARACJE
//-------------------------------

//@brief tworzy podana ilosc procesow potomnych wywylujacych funkcje produceData
//@param howMany - ile procesow stworzyc
void createProducers(int howMany);

//@brief zapisuje dane do pipe'a wskazanego przez pipeDescriptors
void produceData(void);

//@brief tworzy podana ilosc procesow potomnych wywylujacych funkcje consumeData
//@param howMany - ile procesow stworzyc
void createConsumers(int howMany);

//@brief odczytuje dane do pipe'a wskazanego przez pipeDescriptors
void consumeData(void);

//@brief generuje w katalogu ./input/ plik o nazwie PID wywolujacego procesu
//wypelniony losowa iloscia znakow (do 20 max) o losowych wartosciach ASCII od 32 do 126
//@param filePath - tablica w ktorej zostanie zapisana sciezka do nowo utworzonego pliku
void generateFile(char* filePath);

//@brief globalna tablica przechowujaca deskryptory do uzywanego pipe'a
static int pipeDescriptors[2];


//START
//-------------------------------
int main(int argc, char* argv[]) {
    //Kontrola argumentow wywolania
    if (argc != 3)  {
        printf("Niepoprawna ilosc argumentow programu. Wymagane 2: ilosc producentow, ilosc konsumentow\n");
        exit(EXIT_FAILURE);
    }

    char* envMaxProc = getenv("MAXPROC");
    char* envMaxPipe = getenv("MAXPIPE");
    int maxProc = 0;
    int maxPipe = 0;

    if (envMaxProc != NULL) {
        maxProc = atoi(getenv("MAXPROC"));
    }
    else {
        printf("Nie ustawiono zmiennej srodowiskowej MAXPROC\n");
        exit(EXIT_FAILURE);
    }

    if (envMaxPipe != NULL) {
        maxPipe = atoi(getenv("MAXPIPE"));
    }
    else {
        printf("Nie ustawiono zmiennej srodowiskowej MAXPIPE\n");
        exit(EXIT_FAILURE);
    }

    int numOfProducers = atoi(argv[1]);
    int numOfConsumers = atoi(argv[2]);
    if (numOfProducers < 1 || numOfConsumers < 1) {
        printf("Niepoprawne dane wejsciowe\n");
        exit(EXIT_FAILURE);
    }


    if (numOfConsumers + numOfProducers > maxProc || numOfProducers > maxPipe) {
        printf("Podano zbyt duza ilosc programow potomnych do utworzenia. Max: %d\n", maxPipe);
        exit(EXIT_FAILURE);
    }

    //czesc wlasciwa programu
    int returnCode = pipe(pipeDescriptors);
    if (returnCode == -1) {
        perror("Blad przy tworzeniu potoku");
        exit(EXIT_FAILURE);
    }

    createProducers(numOfProducers);
    createConsumers(numOfConsumers);

    close(pipeDescriptors[0]);
    close(pipeDescriptors[1]);
  
    //zamykanie potomnych
    int i;
    for(i = 0; i < numOfProducers + numOfConsumers; i++) {
        int waitStatus;
        pid_t waitID = wait(&waitStatus);
        if (waitID == -1){
            perror("Blad wait()");
            exit(EXIT_FAILURE);
        }
        printf("Zakonczyl sie potomny %d ze statusem %d\n", waitID, waitStatus);
    }

    return 0;
}


//DEFINICJE FUNKCJI
//------------------------------
void createProducers(int howMany) {
    int i;
    for(i = 0; i < howMany; i++) {
        switch(fork()) {

        //fork error
        case -1:
            perror("createProducers fork error");
            exit(EXIT_FAILURE);

        //child process
        case 0:
            produceData();
            exit(EXIT_SUCCESS);

        //parent process
        default:
            break;
        }
    }
}


void produceData(void) {
    pid_t thisProc = getpid();
    srand(thisProc);

    //zamyka czytanie (stdin)
    int returnCode;
    returnCode = close(pipeDescriptors[0]);
    if (returnCode == -1) {
        perror("produceData close([0]) error");
        exit(EXIT_FAILURE);
    }

    char filePath[32];
    generateFile(filePath);

    FILE* file;
    file = fopen(filePath, "r");
    if (file == NULL) {
        perror("produceData fopen error");
        exit(EXIT_FAILURE);
    }

    printf("PRODUCENT %d: start produkcji\n", thisProc);

    char singleChar;
    ssize_t bytesWriten;
    while ((singleChar = getc(file)) != EOF) {
        bytesWriten = write(pipeDescriptors[1], &singleChar, sizeof(char));
        if (bytesWriten == -1) {
            perror("produceData write error");
            exit(EXIT_FAILURE);
        }
        printf("-->PRODUCENT %d: zapisalem %ldB danych: %c\n", thisProc, bytesWriten, singleChar);
    }

    fclose(file);

    //zamyka pisanie (stdout)
    returnCode = close(pipeDescriptors[1]);
    if (returnCode == -1) {
        perror("produceData close([1]) error");
        exit(EXIT_FAILURE);
    }
}


void createConsumers(int howMany) {
    int i;
    for(i = 0; i < howMany; i++) {
        switch(fork()) {

        //fork error
        case -1:
            perror("createConsumers fork error");
            exit(EXIT_FAILURE);

        //child process
        case 0:
            consumeData();
            exit(EXIT_SUCCESS);

        //parent process
        default:
            break;
        }
    }
}


void consumeData(void) {
    //zamyka pisanie
    int returnCode;
    returnCode = close(pipeDescriptors[1]);
    if (returnCode == -1) {
        perror("consumeData close([1]) error");
        exit(EXIT_FAILURE);
    }

    char outputFilePath[32];
    sprintf(outputFilePath, "output/%d.data", getpid());

    FILE* file;
    file = fopen(outputFilePath, "w");
    if (file == NULL) {
        perror("consumeData fopen error");
        exit(EXIT_FAILURE);
    }

    char singleChar;
    ssize_t bytesRead;
    pid_t thisProc = getpid();
    printf("KONSUMENT %d: start konsumpcji\n", thisProc);

    while(1) {
        bytesRead = read(pipeDescriptors[0], &singleChar, sizeof(char));
        if (bytesRead == -1) {
            perror("consumeData error");
            exit(EXIT_FAILURE);
        }
        else if (bytesRead == 0 || singleChar == EOF)
            break;
        else {
            printf("<--KONSUMENT %d: pobralem %ldB danych: %c\n", thisProc, bytesRead, singleChar);
            putc(singleChar, file);
        }
    }
	
	fclose(file);

    //zamyka czytanie
    returnCode = close(pipeDescriptors[0]);
    if (returnCode == -1) {
      perror("produceData close([0]) error");
      exit(EXIT_FAILURE);
    }
}


void generateFile(char* filePath) {
  FILE* file;
  int numberOfChars = rand() % 19 + 1;

  sprintf(filePath, "./input/%d.data", getpid());

  file = fopen(filePath, "w");
  if (file == NULL) {
      perror("generateFile fopen error");
      exit(EXIT_FAILURE);
  }

  int i;
  char printableAscii;
  for(i = 0; i < numberOfChars; i++) {
    printableAscii = rand() % (126-32) + 32;
    fprintf(file, "%c", printableAscii);
  }

  fclose(file);
}
