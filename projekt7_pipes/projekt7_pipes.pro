TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    pipe/main.c \
    pipe/main.c \
    mkfifo/main.c \
    mkfifo/producer.c \
    mkfifo/consumer.c

DISTFILES += \
    pipe/cleaner.sh \
    pipe/start.sh \
    pipe/show_result.sh \
    mkfifo/cleaner.sh \
    mkfifo/show_result.sh \
    mkfifo/start.sh \
    mkfifo/makefile
