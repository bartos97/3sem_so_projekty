TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++0x -pthread
LIBS += -pthread

SOURCES += \
        main.c \
    utils.c \
    utils_array.c

HEADERS += \
    utils.h \
    utils_array.h
