#include "utils_array.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


//FILL ARRAY
//------------------------------------------------
void fillArray(int* arr, unsigned int rows, unsigned int cols) {
    unsigned int i,j;
    time_t timer;

    srand(time(&timer));

    for(i=0; i<rows; i++) {
        for (j=0; j<cols; j++) {
            *(++arr) = rand() % 10;
        }
    }
}


//PRINT ARRAY
//------------------------------------------------
void printArray(int* arr, unsigned int rows, unsigned int cols) {
    unsigned int i,j;

    for(i=0; i<rows; i++) {
        for (j=0; j<cols; j++) {
            printf("%d ", *(arr++));
        }
        printf("\n");
    }
}


//SUM ARRAY
//------------------------------------------------
int sumArray(int* begin, unsigned int howMany) {
    int sum=0;
    for (unsigned int i=0; i<howMany; i++) {
        sum += *begin;
        begin++;
    }
    return sum;
}
