#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/errno.h>

#include "utils_array.h"
#include "utils.h"



//DEKLARACJE FUNKCJI I TYPOW
//-------------------------
void* threadFunction(void* _args);

typedef struct _tf_args {
    int* dataBegin;
    unsigned int howManyCells;
    int* result;
} ThreadArgs;



//FUNKCJA GLOWNA
//--------------------------
int main() {
    pthread_t thread1, thread2;
    int threadOpsCode = 1;
    int row1sum, row2sum;

    //Generowanie tablicy
    int arr[2][10];
    fillArray(&arr[0][0], 2, 10);
    printArray(&arr[0][0], 2, 10);


    //1 watek
    ThreadArgs t1args = {&arr[0][0], 10, &row1sum};
    threadOpsCode = pthread_create(&thread1, NULL, threadFunction, &t1args);
    if (threadOpsCode) fatalError("Blad podczas tworzenia watku 1");


    //2 watek
    ThreadArgs t2args = {&arr[1][0], 10, &row2sum};
    threadOpsCode = pthread_create(&thread2, NULL, threadFunction, &t2args);
    if (threadOpsCode) fatalError("Blad podczas tworzenia watku 2");


    //watek glowny:

    //przylaczanie watkow
    threadOpsCode = pthread_join(thread1, NULL);
    if (threadOpsCode) fatalError("Blad podczas przylaczania watku 1");

    printf("Suma 1go rzedu: %d\n", row1sum);


    threadOpsCode = pthread_join(thread2, NULL);
    if (threadOpsCode) fatalError("Blad podczas przylaczania watku 2");

    printf("Suma 2go rzedu: %d\n", row2sum);


    //odlaczanie watkow
    //threadOpsCode = pthread_detach(thread1);
    //if (threadOpsCode)  fatalError("Blad podczas odlaczania watku 1");

    //threadOpsCode = pthread_detach(thread2);
    //if (threadOpsCode) fatalError("Blad podczas odlaczania watku 2");


    //wynik koncowy
    printf("Calkowita suma: %d\n", row1sum + row2sum);

    exit(EXIT_SUCCESS);
}



//DEFINICJE FUNKCJI
//-------------------------
void* threadFunction(void* _args) {
    printf("Wlasnie dziala watek %lu\n", pthread_self());
    ThreadArgs* arg = (ThreadArgs*)_args;
    *(arg->result) = sumArray(arg->dataBegin, arg->howManyCells);
    pthread_exit(0);
}
