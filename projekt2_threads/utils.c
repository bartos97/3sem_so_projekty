#include "utils.h"

#include <stdio.h>
#include <stdlib.h>


//FATAL ERROR
//------------------------------------------------
void fatalError(const char* str) {
    printf("FATAL ERROR: %s\n", str);
    exit(EXIT_FAILURE);
}
