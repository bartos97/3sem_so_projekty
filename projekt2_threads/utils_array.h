#ifndef UTILS_ARRAY_H
#define UTILS_ARRAY_H


//Wypelnia podana tablce losowymi intami
//arr - wskaznik do poczatku tablicy
//rows - ilosc rzedow w tablicy
//cols - ilosc kolumn w tablicy
void fillArray(int* arr, unsigned int rows, unsigned int cols);


//Wyswietla cala tablice
//arr - wskaznik do poczatku tablicy
//rows - ilosc rzedow w tablicy
//cols - ilosc kolumn w tablicy
void printArray(int* arr, unsigned int rows, unsigned int cols);


//Sumuje elementy tablicy
//begin - wskaźnik do elementu tablicy od którego zacząć sumowanie
//howMuch - ile kolejnych elementów zsumować
int sumArray(int* begin, unsigned int howMany);


#endif // UTILS_ARRAY_H
