#include "sem_utils.h"

#include <limits.h>


int main(int argc, char** argv) {

    //Walidacja danych
    if (argc != 4) {
        printf("Bledne wywolanie programu\n");
        exit(EXIT_FAILURE);
    }

    char* envProcLimit = getenv("PROCLIMIT");
    int maxProc = 0;
    if (envProcLimit != NULL) {
        maxProc = atoi(getenv("PROCLIMIT"));
    }
    else {
        printf("Nie ustawiono zmiennej srodowiskowej PROCLIMIT\n");
        exit(EXIT_FAILURE);
    }

    char* envActiveProc = getenv("ACTIVEPROC");
    int procLimit = 0;
    if (envActiveProc != NULL) {
        procLimit = maxProc - atoi(getenv("ACTIVEPROC")) - 100;
        printf("Limit procesow: %d\n", procLimit);
    }
    else {
        printf("Nie ustawiono zmiennej srodowiskowej ACTIVEPROC\n");
        exit(EXIT_FAILURE);
    }

    char* childProc = argv[1];
    int childProcAmount = atoi(argv[2]);
    if (childProcAmount > procLimit || childProcAmount < 1) {
        printf("Podana ilosc procesow potomnych jest niepoprawna\n");
        exit(EXIT_FAILURE);
    }

    char* criticSectionsAmount = argv[3];
    int criticInt = atoi(criticSectionsAmount);
    if (criticInt > INT_MAX || criticInt < 1) {
        printf("Podana ilosc sekcji krytycznych jest niepoprawna\n");
        exit(EXIT_FAILURE);
    }


    //Tworzenie semafora
    int semID = semAccess(1, 'Q');
    semSet(semID, 0, 1);


    //tworzenie programow potomnych
    pid_t waitID, forkID, execID = 0;
    int i,j,x;

    for (i=0; i<childProcAmount; i++) {
        switch(forkID = fork()) {
        case 0:
            execID = execl(childProc, childProc, criticSectionsAmount, NULL);
            if (execID == -1) {
                perror("Blad execl'a");
                exit(EXIT_FAILURE);
            }
            break;
        case -1:
            perror("Blad forka");
            for (j=0; j<i; j++) {
                waitID = wait(&x);
                printf("Zakonczyl sie %d ze statusem: %d\n", waitID, x);
            }
            semDelete(semID);
            exit(EXIT_FAILURE);
        default:
            break;
        }
    }


    for (i=0; i<childProcAmount; i++) {
        waitID = wait(&x);
        printf("Zakonczyl sie %d ze statusem: %d\n", waitID, x);
    }


    //Usuwanie semafora
    semDelete(semID);
	
	return 0;
}
