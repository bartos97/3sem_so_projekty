#ifndef SEM_UTILS_H
#define SEM_UTILS_H


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/errno.h>


//Returns a key_t type System V IPC key
key_t getIPCkey(int id);

//Creates new semaphore set or access existing associated with given keyID
// RET: id of semaphore set
// IN:  num - number of semaphores to create in set
//      keyID - 8bit integer used to generate IPC key
int semAccess(int num, char keyID);

//Set value to semaphore
void semSet(int semID, unsigned short whichSem, int value);

//Increase or decrease semaphore value
// mode == 'v' => increase by 1
// mode == 'p' => decrease by 1
void semChange(int semID, unsigned short whichSem, char mode);

//Delete semaphore
void semDelete(int semID);


#endif // SEM_UTILS_H
