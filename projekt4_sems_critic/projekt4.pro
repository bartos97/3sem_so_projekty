TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    sem_utils.c \
    main.c \

HEADERS += \
    sem_utils.h
