#include "sem_utils.h"


void showYourself(void) {
    printf("\tCHILDPROC %d: ", getpid());
}


void loader(void) {
    for (int j=0; j<=100; j+=50) {
        showYourself();
        printf("Pracuje... %d%%\n", j);
        sleep(1);
    }
}


int main(int argc, char** argv) {

    //Konrola argumentow
    if (argc != 2) {
        showYourself();
        printf("Bledn wywolanie programu\n");
        exit(EXIT_FAILURE);
    }

    int criticSectionsAmount = atoi(argv[1]);


    //Dostep do semafora
    showYourself();
    int semID = semAccess(1, 'Q');


    //Petla sekcji krytycznych
    int i;
    for (i=0; i<criticSectionsAmount; i++) {
        showYourself();
        printf("Czekam na wejscie do sekcji krytycznej...\n");

        showYourself();
        semChange(semID, 0, 'p');

        showYourself();
        printf("Wykonuje sekcje krytyczna...\n");
        showYourself();
        printf("Wartosc semafora %d: %d, ilosc czekajacych %d \n", semID, semctl(semID, 0, GETVAL), semctl(semID,0,GETNCNT));
        loader();

        showYourself();
        printf("Zakonczylem sekcje krytyczna\n");

        showYourself();
        semChange(semID, 0, 'v');
    }

}
