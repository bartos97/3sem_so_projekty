#include "sem_utils.h"


//getIPCkey
//-------------------------------------------------------------
key_t getIPCkey(int id) {
    key_t key = ftok(".", id);
    if (key == -1) {
        perror("From getIPCkey ftok error");
        exit(EXIT_FAILURE);
    }
    return key;
}


//semAccess
//-------------------------------------------------------------
int semAccess(int num, char keyID) {
    key_t key = getIPCkey(keyID);

    int semID = semget(key, num, 0600|IPC_CREAT);
    if (semID == -1)  {
        perror("From semAccess semget error");
        exit(EXIT_FAILURE);
    }
    else {
        printf("Semafor %d uruchomione semget\n", semID);
        return semID;
    }
}


//semSet
//-------------------------------------------------------------
void semSet(int semID, unsigned short whichSem, int value) {
    if ( semctl(semID, whichSem, SETVAL, value) == -1) {
        perror("From semSet semctl error");
        exit(EXIT_FAILURE);
    }
    else {
        printf("Semafor %d ustawiony na %d\n", semID, value);
    }
}


//semChange
//-------------------------------------------------------------
void semChange(int semID, unsigned short whichSem, char mode) {
    const char* msg;
    struct sembuf bufor_sem;
    bufor_sem.sem_num = whichSem;
    bufor_sem.sem_flg = SEM_UNDO;

    switch(mode) {
        case 'v':
            bufor_sem.sem_op = 1;
            msg = "Semafor %d podniesiony\n";
            break;
        case 'p':
            bufor_sem.sem_op = -1;
            msg = "Semafor %d obnizony\n";
            break;
        default:
            printf("semChange error: podano bledny argument (dozwolone 'v' lub 'p')\n");
            return;
    }

    if ( semop(semID, &bufor_sem, 1) == -1 ) {
        perror("From semChange semop error");
        exit(EXIT_FAILURE);
    }
    else {
        printf(msg, semID);
    }
}


//semDelete
//-------------------------------------------------------------
void semDelete(int semID) {
    if ( semctl(semID, 0, IPC_RMID) == -1 ) {
        perror("From semDelete semctl error");
        exit(EXIT_FAILURE);
    }
    else {
        printf("Semafor %d usuniety\n", semID);
    }
}
