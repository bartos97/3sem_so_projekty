TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c \
    utils_sem.c

HEADERS += \
    utils_sem.h
