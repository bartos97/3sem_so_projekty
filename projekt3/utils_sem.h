#ifndef UTILS_SEM_H
#define UTILS_SEM_H


// Tworzy nowy semafor
int semCreateNew(void);

void ustaw_semafor(void);
void semafor_p(void);
void semafor_v(void);
void usun_semafor(void);


#endif // UTILS_SEM_H
