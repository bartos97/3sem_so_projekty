#include "sem_utils.h"


//getIPCkey
//-------------------------------------------------------------
key_t getIPCkey(int id) {
    key_t key = ftok(".", id);
    if (key == -1) {
        perror("From getIPCkey ftok error");
        exit(EXIT_FAILURE);
    }
    return key;
}


//semAccess
//-------------------------------------------------------------
int semAccess(int num, char keyID) {
    key_t key = getIPCkey(keyID);

    int semID = semget(key, num, 0600|IPC_CREAT);
    if (semID == -1)  {
        perror("From semAccess semget error");
        exit(EXIT_FAILURE);
    }
    else {
        printf("Semafor %d uruchomione semget\n", semID);
        return semID;
    }
}


//semSet
//-------------------------------------------------------------
void semSet(int semID, unsigned short whichSem, int value) {
    if ( semctl(semID, whichSem, SETVAL, value) == -1) {
        perror("From semSet semctl error");
        exit(EXIT_FAILURE);
    }
    else {
        printf("Semafor %d #%d ustawiony na %d\n", semID, whichSem, value);
    }
}


//semChange
//-------------------------------------------------------------
void semChange(int semID, unsigned short whichSem, char mode) {
    const char* msg;
    struct sembuf bufor_sem;
    bufor_sem.sem_num = whichSem;
    bufor_sem.sem_flg = SEM_UNDO;

    switch(mode) {
        case 'v':
            bufor_sem.sem_op = 1;
            msg = "Semafor %d #%d podniesiony\n";
            break;
        case 'p':
            bufor_sem.sem_op = -1;
            msg = "Semafor %d #%d obnizony\n";
            break;
        default:
            printf("semChange error: podano bledny argument (dozwolone 'v' lub 'p')\n");
            return;
    }

    int semopCode;
    while (1) {
        semopCode = semop(semID, &bufor_sem, 1);
        if (semopCode == 0) break;
        else if (errno == 4) continue;
        else {
            perror("From semChange semop error");
            exit(EXIT_FAILURE);
        }
    }
    printf(msg, semID, whichSem);
}


//semDelete
//-------------------------------------------------------------
void semDelete(int semID) {
    if ( semctl(semID, 0, IPC_RMID) == -1 ) {
        perror("From semDelete semctl error");
        exit(EXIT_FAILURE);
    }
    else {
        printf("Semafor %d usuniety\n", semID);
    }
}


//shmemCreate
//-------------------------------------------------------------
int shmemCreate(size_t size, char keyID) {
    key_t key = getIPCkey(keyID);

    int shmemID = shmget(key, size, 0600|IPC_CREAT|IPC_EXCL);
    if (shmemID == -1)  {
        perror("From shmemCreate shmget error");
        exit(EXIT_FAILURE);
    }
    else {
        printf("Shared memory %d: utworzono segment\n", shmemID);
        return shmemID;
    }
}


//shmemAccess
//-------------------------------------------------------------
int shmemAccess(char keyID) {
    key_t key = getIPCkey(keyID);

    int shmemID = shmget(key, 0, 0);
    if (shmemID == -1)  {
        perror("From shmemAccess shmget error");
        exit(EXIT_FAILURE);
    }
    else {
        printf("Shared memory %d: przyznano dostep do segmentu\n", shmemID);
        return shmemID;
    }
}


//shmemGetPtr
//-------------------------------------------------------------
void* shmemGetPtr(int shmemID) {
    void* shmemAddr = shmat(shmemID, 0, 0);
    if (shmemAddr == -1)  {
        perror("From shmemGetPtr shmat error");
        exit(EXIT_FAILURE);
    }
    else {
        printf("Shared memory %d: pobrano wskaznik\n", shmemID);
        return shmemAddr;
    }
}


//shmemRemove
//-------------------------------------------------------------
void shmemRemove(int shmemID, void* shmemPtr) {
    if (shmctl(shmemID, IPC_RMID, 0) == -1) {
        perror("From shmemRemove shmctl error");
        exit(EXIT_FAILURE);
    }

    if (shmdt(shmemPtr) == -1) {
        perror("From shmemRemove shmdt error");
        exit(EXIT_FAILURE);
    }

    printf("Shared memory %d: usunieto i odlaczono segment\n", shmemID);
}
