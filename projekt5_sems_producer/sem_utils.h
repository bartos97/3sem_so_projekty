#ifndef SEM_UTILS_H
#define SEM_UTILS_H


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/errno.h>


//Returns a key_t type System V IPC key
key_t getIPCkey(int id);

//Creates new semaphore set or access existing associated with given keyID
// RET: id of semaphore set
// IN:  num - number of semaphores to create in set
//      keyID - 8bit integer used to generate IPC key
int semAccess(int num, char keyID);

//Set value to semaphore
void semSet(int semID, unsigned short whichSem, int value);

//Increase or decrease semaphore value
// mode == 'v' => increase by 1
// mode == 'p' => decrease by 1
void semChange(int semID, unsigned short whichSem, char mode);

//Delete semaphore
void semDelete(int semID);


//Uses shmget to create new shared memory segment
// RET: id of shared memory segment
// IN:  size - size of memory to be created in bytes
//      keyID - 8bit integer used to generate IPC key
int shmemCreate(size_t size, char keyID);

//Uses shmget to access existing shared memory segment associated with keyID
int shmemAccess(char keyID);

//Get address to previously created shared memory segment
void* shmemGetPtr(int shmemID);

//Detach and remove given shared memory segment
void shmemRemove(int shmemID, void* shmemPtr);


#endif // SEM_UTILS_H
