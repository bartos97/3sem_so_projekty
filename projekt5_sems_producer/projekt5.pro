TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    sem_utils.c \
    producer.c \
    consumer.c

HEADERS += \
    sem_utils.h
