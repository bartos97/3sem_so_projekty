#include "sem_utils.h"


int main() {
    srand(time(NULL));

    //Klucz do zbioru semaforow i pamieci dzielonej
    char keyID = 'z';

    //Dostep do pamieci dzielonej
    int bufferID = shmemAccess(keyID);
    char* bufferPtr = shmemGetPtr(bufferID);

    //Dostep do semaforow
    int semID = semAccess(2, keyID);

    //Otwarcie pliku do zapisu
    const char* filePath = "./consumer_data.txt";
    FILE* outputFile = fopen(filePath, "w");
    if (outputFile == NULL) {
        printf("Nie udało się otworzyć pliku %s\n", filePath);
        exit(EXIT_FAILURE);
    }

    //Odczyt znaków z pamieci dzielonej i zapis do pliku
    char resource;
    while(1) {
        semChange(semID, 1, 'p'); //opuszczenie semafora (konsumenta)

        printf("KONSUMENT: Dzialam...");
        fflush(stdout);
        sleep(rand() % 5);
        printf("\r");

        resource = *bufferPtr;
        if (resource == EOF) {
            //fputc('\n', outputFile);
            break;
        }
        fputc(resource, outputFile);
        printf("KONSUMENT: Pobrano do pliku: %c\n", resource);

        semChange(semID, 0, 'v'); //podnoszenie semafora (producenta)
    }

    //Zamykanie pliku
    fclose(outputFile);
    return 0;
}
