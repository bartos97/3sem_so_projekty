#include "sem_utils.h"


int main() {
    srand(time(NULL));

    //Klucz do zbioru semaforow i pamieci dzielonej
    char keyID = 'z';

    //Otwarcie pamieci dzielonej
    int bufferID = shmemCreate(sizeof(char), keyID);
    char* bufferPtr = shmemGetPtr(bufferID);

    //Utworzenie semaforow
    int semID = semAccess(2, keyID);
    semSet(semID, 0, 1); //mutex producent
    semSet(semID, 1, 0); //mutex konsumenta

    //Otwarcie pliku z danymi
    const char* filePath = "./producer_data.txt";
    FILE* inputFile = fopen(filePath, "r");
    if (inputFile == NULL) {
        printf("Nie udało się otworzyć pliku %s\n", filePath);
        shmemRemove(bufferID, bufferPtr);
        semDelete(semID);
        exit(EXIT_FAILURE);
    }

    //Odczyt znaków z pliku do pamieci dzielonej
    char resource;
    while(!feof(inputFile)) {
        semChange(semID, 0, 'p'); //opuszczenie semafora (producenta)

        printf("PRODUCENT: Dzialam...");
        fflush(stdout);
        sleep(rand() % 5);
        printf("\r");

        resource = fgetc(inputFile);
        if (resource != EOF)
            printf("PRODUCENT: Wyprodukowano z pliku: %c\n", resource);
        *bufferPtr = resource;

        semChange(semID, 1, 'v'); //podniesienie semafora (konsumenta)
    }

    //Zamykanie pamieci dzielonej, pliku, semaforow
    shmemRemove(bufferID, bufferPtr);
    semDelete(semID);
    fclose(inputFile);
    return 0;
}
