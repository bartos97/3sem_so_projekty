#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <sys/types.h>
#include <sys/errno.h>
#include <sys/wait.h>



//FUNKCJA GLOWNA
//Zmodyfikować poprzedni program tak, aby komunikaty procesów potomnych były wypisywane przez program
//uruchamiany przez funkcję exec(). Ile teraz powstaje procesów i dlaczego?.
//--------------------------
int main () {
    pid_t waitID, forkID, execID = 0;
	
	char treeCmd[20];
    sprintf(treeCmd, "pstree -phl %d", getpid());

    printf("Petla z forkami i execami:\n");
    printf("--------------------------------\n");

	int i;
    for (i=1; i<4; i++) {
        printf("%d. Start forka\n", i);
		
        switch(forkID = fork()) {
        case 0:
            //execID = execl("./zad1.exe", "./zad1.exe", "-s", NULL);
			execID = execl("/usr/bin/wc", "wc", "-c", "zad3.c",  NULL);
			if (execID == -1) {
				perror("Blad execl'a: ");
				exit(-1);
			}	
            break;
        case -1:
            perror("Blad forka");
            exit(1);
        default:            
            break;
        }
    }
	
	//Na podstawie wyników narysować „drzewo genealogiczne” tworzonych procesów
    system(treeCmd);
	printf("\n");
	
	int x;
	for (i=0; i<3; i++) {
		waitID = wait(&x);
        printf("Zakonczyl sie %d ze statusem: %d\n", waitID, x);
	}

}
