#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <sys/types.h>
#include <sys/errno.h>
#include <sys/wait.h>



//NAGLOWKI FUNKCJI
//--------------------------
void printIDs(void);
void printProcIDs(void);
void printProcTree(pid_t*);



//FUNKCJA GLOWNA
//--------------------------
int main (int argc, char** argv) {

    //Napisać program wypisujący identyfikatory UID, GID, PID, PPID dla danego procesu.
    printf("Identyfikatory tego procesu:\n");
    printf("--------------------------------\n");
    printIDs();

    if (argc == 2 ) return 0;


    //Wywołać funkcję fork() 3 razy i wypisać powyzsze identyfikatory dla wszystkich procesów potomnych
    pid_t waitID, forkID;
    char treeCmd[20];
    sprintf(treeCmd, "pstree -phl %d", getpid());

    printf("\nPetla z forkami:\n");
    printf("--------------------------------\n");

    int i;
    for (i=0; i<3; i++) {
        printf("%d. Start forka\n", i);
		
        switch(forkID = fork()) {
        case 0:
			//fork();
            printProcIDs();
            break;
        case -1:
            perror("Blad forka");
            exit(1);
        default:
            //waitID = wait(NULL);
            //printf("Zakonczyl sie %d\n" ,waitID);
            break;
        }
    }


    //Na podstawie wyników narysować „drzewo genealogiczne” tworzonych procesów
    system(treeCmd);

    for (i=0; i<3; i++) waitID = wait(NULL);

    exit(0);
}



//DEFINICJE FUNKCJI
//--------------------------
inline void printIDs(void) {
    printf("UID (user): %d\n", getuid());
    printf("GID (user's group): %d\n", getgid());
    printf("PID (process): %d\n", getpid());
    printf("PPID (parent process): %d\n", getppid());
}


inline void printProcIDs(void) {
    printf("\tPID:\t %d\n", getpid());
    printf("\tPPID:\t %d\n", getppid());
}

void printProcTree(pid_t* base) {
    char command[20];
    sprintf(command, "pstree -p %d", *base);
    system(command);
}
