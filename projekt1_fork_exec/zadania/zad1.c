#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <sys/types.h>
#include <sys/errno.h>
#include <sys/wait.h>



//NAGLOWKI FUNKCJI
//--------------------------
void printIDsLong(void);
void printIDsShort(void);



//FUNKCJA GLOWNA
//Napisać program wypisujący identyfikatory UID, GID, PID, PPID dla danego procesu.
//--------------------------
int main (int argc, char** argv) {
    if (argc == 2) {
        //&& argv[1] == "s"
        printIDsShort();
    }
    else printIDsLong();
}



//DEFINICJE FUNKCJI
//--------------------------
inline void printIDsLong(void) {
    printf("Identyfikatory tego procesu:\n");
    printf("UID (user): %d\n", getuid());
    printf("GID (user's group): %d\n", getgid());
    printf("PID (process): %d\n", getpid());
    printf("PPID (parent process): %d\n", getppid());
}

inline void printIDsShort(void) {
    pid_t id = getpid();
    printf("\t%d:\tUID: %d\n", id, getuid());
    printf("\t%d:\tGID: %d\n", id, getgid());
    printf("\t%d:\tPID: %d\n", id, getpid());
    printf("\t%d:\tPPID: %d\n", id, getppid());
}
