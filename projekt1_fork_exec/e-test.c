#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv){
	int i,id,x,w;
	char znak;
	fprintf(stdout,"Moj PID %d\n", getpid());
	fprintf(stdout,"Terz utworze potomka\n");
	id = fork();
	if(id == 0){
		fprintf(stdout,"Podmieniam swoj segment instrukcji\n");
		execl("/usr/bin/gnome-terminal", "gnome-terminal", NULL);
		/*sciezka, zazwa pograu, opcje, calosc konczona jest NULL'em*/
	}
	fprintf(stdout, "Potomek ma PID=%d\n", id);
	for(i = 0; i < 3; i++){
		fprintf(stdout,"Wcisnij [enter]");
		fscanf(stdin,"%c",&znak);		
	}
	fprintf(stdout,"Teaz czekam na zakonczenie potomka\n");
	w = wait(&x);
	fprintf(stdout, "Skonczyl sie potomny PID=%d\n",w);
	fprintf(stdout,"Koniec maierzystego PID=%d\n",getpid());
}

