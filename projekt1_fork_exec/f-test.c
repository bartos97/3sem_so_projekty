#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
	int i,id,x,w;
	fprintf(stdout,"Moj PID %d\n",getpid());
	fprintf(stdout,"Teraz utworze potomka\n");
	id = fork();
	if(id == 0){
		fprintf(stdout,"To pisze proces potomny id=%d PID=%d\n",id,getpid());
	//	sleep (8);
	}
	else {
		fprintf(stdout,"To pisze proces macierzysty id=%d PID=%d\n",id,getpid());
		fprintf(stdout,"Czekam na zakonczenie potomka\n");
		w = wait(&x);
		fprintf(stdout,"Skonczyl sie potomny PID=%d\n",w);
		
	
	}
	fprintf(stdout, "x wynosi: %d\n", x);
	
	fprintf(stdout,"A to napisza oba procescy PID=%d\n",getpid());
	exit (1);
}

