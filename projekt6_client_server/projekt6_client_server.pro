TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    client.c \
    server.c \
    utils.c

HEADERS += \
    utils.h
