#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>
#include "utils.h"


void closeServer(int _flagSuccess);
void signalHandler(int _signal);


static int accessKey = 2019;
static int queueID;


int main(){
    int msgReceiveReturn;
    int msgSendReturn;
    msgQueueCommunication communicator;
    msgQueueProcType communicationProcess;

    queueID = msgQueueCreate(accessKey, 1);
    printf("Serwer: PID = %d ID Kolejki = %d\n", getpid(), queueID);

    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGQUIT, signalHandler);

    //TEST: zmiana rozmiaru kolejki
//    struct msqid_ds msgStruct;
//    msgStruct.msg_qbytes = sizeof (msgQueueCommunication) * 2;
//    if (msgctl(queueID, IPC_SET, &msgStruct) == -1) {
//        perror("Serwer test msg queue size change");
//        closeServer(0);
//    }


    while (1) {
        printf("Serwer: Czekam na komunikat...\n");
        communicator.receiver = communicationProcess = SERVER;

        // Serwer "wisi" jesli nastapilo przerwanie
        do {
            msgReceiveReturn = msgrcv(queueID, &communicator, MSGQUEUE_MSG_SIZE, communicator.receiver, MSG_NOERROR);
        } while (msgReceiveReturn == -1 && errno == EINTR);

        // jesli blad inny niz przerwanie to koniec
        if (msgReceiveReturn == -1) {
            perror("Serwer: msgrcv error");
            closeServer(0);
        }

        printf("Serwer: Odebrano: \"%s\" od %ld\n", communicator.mtext, communicator.sender);

        //modyfikacja wiadomosci
        size_t msize = strlen(communicator.mtext);
        for(size_t i = 0; i < msize; i++) {
            communicator.mtext[i] = toupper(communicator.mtext[i]);
        }

        communicator.receiver = communicator.sender;
        communicator.sender = communicationProcess = SERVER;
        printf("Serwer: Rozpoczynam wysylanie... %s -> %ld \n", communicator.mtext, communicator.receiver);

        //wyslanie wiadomosci
        //brak potrzeby sprawdzania miejsca w kolejce - klient dba o to aby nie wysylac jesli nie ma miejsca na odpowiedz

        if (getpgid(communicator.receiver) == -1) {
            printf("Serwer -/-> %ld: Odbiorca nie istnieje!\n", communicator.receiver);
            continue;
        }

        do {
            msgSendReturn = msgsnd(queueID, &communicator, MSGQUEUE_MSG_SIZE, IPC_NOWAIT);
        } while (msgSendReturn == -1 && (errno == EINTR || errno == EAGAIN));

        if (msgSendReturn == -1) {
            perror("Serwer: msgsnd error");
            closeServer(0);
        }

        printf("Serwer: WYSLANO %s -> %ld \n", communicator.mtext, communicator.receiver);
    }
    
    closeServer(1);
}


void closeServer(int _flagSuccess) {
    msgQueueRemove(queueID);
    printf("Serwer: KONIEC PRACY\n");
    if (_flagSuccess) exit(EXIT_SUCCESS);
    else exit(EXIT_FAILURE);
}


void signalHandler(int _signal) {
    closeServer(0);
}
