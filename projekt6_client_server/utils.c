#include "utils.h"

//const size_t MSGQUEUE_MSG_SIZE = sizeof (msgQueueCommunication) - sizeof (long);
const size_t MSGQUEUE_MSG_SIZE = sizeof(long) + MAX_MSG_SIZE;

key_t getIPCkey(int id) {
    key_t key = ftok(".", id);
    if (key == -1) {
        perror("From getIPCkey ftok error");
        exit(EXIT_FAILURE);
    }
    return key;
}


int msgQueueCreate(int _key, int _flagEncodeKey) {
    int id;
    key_t key;

    if (_flagEncodeKey) key = getIPCkey(_key);
    else                key = _key;

    id = msgget(key, IPC_CREAT | IPC_EXCL | 0660);
    if( id == -1) {
        perror("From utils::msgQueueCreate, msgget error.\n");
        exit(EXIT_FAILURE);
    }

    return id;
}


int msgQueueAccess(int _key, int _flagEncodeKey, const char* customErrorMsg) {
    int id;
    key_t key;

    if (_flagEncodeKey) key = getIPCkey(_key);
    else                key = _key;

    id = msgget(key, 0);
    if( id == -1) {
        perror("From utils::msgQueueAccess, msgget error.\n");
        printf(customErrorMsg);
        printf("\n");
        exit(EXIT_FAILURE);
    }

    return id;
}


void msgQueueGetInfo(int _id, struct msqid_ds* _struct, int _flagHardExit) {
    if (msgctl(_id, IPC_STAT, _struct) == -1) {
        perror("From util::msgQueueGetInfo msgctl error");
        if (_flagHardExit) exit(EXIT_FAILURE);
        else pthread_exit(0);
    }
}


void msgQueueRemove(int _id) {
    if ( msgctl(_id, IPC_RMID, 0) == -1 ) {
        perror("Serwer: Blad usuniecia kolejki.\n");
        exit(EXIT_FAILURE);
    }
}
