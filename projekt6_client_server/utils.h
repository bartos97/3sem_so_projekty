#ifndef UTILS_H
#define UTILS_H


#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>

#define MAX_MSG_SIZE 4000
extern const size_t MSGQUEUE_MSG_SIZE;

//Enum zawierający ID rodzaju procesu do komunikacji
typedef enum communicationProcessType {
    SERVER = 1,
    CLIENT = 2
} msgQueueProcType;

//Struktura używana do wysyłania komunikatów
typedef struct communication {
    long int receiver;
    long int sender;
    char mtext[MAX_MSG_SIZE];
} msgQueueCommunication;

//Genereuje klucz ftok'iem na podstawie podanego inta (most signifant 8bits sie liczy)
key_t getIPCkey(int id);

//Utworz kolejke, jesli juz istnieje konczy bledem
//@param _key -  klucz dostępu
//@param _flagEncodeKey - jeśli true to klucz zostanie wygenerowany ftokiem na podstawie podanego znaku
int msgQueueCreate(int _key, int _flagEncodeKey);

//Zwraca ID istniejacej kolejki, jesli nie istnieje to konczy bledem
//@param _key -  klucz dostępu
//@param _flagEncodeKey - jeśli true to klucz zostanie wygenerowany ftokiem na podstawie podanego znaku
//@param customErrorMsg - wiadomosc wyswietlona w razie bledu
int msgQueueAccess(int _key, int _flagEncodeKey, const char* customErrorMsg);

//Pobiera dane o kolejce
//@param _id -  klucz dostępu
//@param _struct - wskaznik do struktury przechowujacej dane o kolejce
//@param _flagHardExit - jesli true to EXIT_FAILURE jesli nie to thread_exit
void msgQueueGetInfo(int _id, struct msqid_ds* _struct, int _flagHardExit);

//Usun kolejke o podanym ID
void msgQueueRemove(int _id);


#endif // UTILS_H
