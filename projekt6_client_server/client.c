#include <unistd.h>
#include <string.h>
#include "utils.h"


void* senderFunc(void* _args);
void* receiverFunc(void* _args);


static int accessKey = 2019;
static int queueID;
static int flagReceived = 1;


int main() {
    int threadOpsCode;
    pthread_t threadSender, threadReceiver;
	
    queueID = msgQueueAccess(accessKey, 0, "Nalezy najpierw otworzyc serwer");
    printf("Klient: PID = %d ID Kolejki = %d\n", getpid(), queueID);

    threadOpsCode = pthread_create(&threadSender, NULL, senderFunc, NULL);
    if (threadOpsCode) {
        perror("Klient: blad przy tworzeniu watku nadawcy");
        exit(EXIT_FAILURE);
    }

    threadOpsCode = pthread_create(&threadReceiver, NULL, receiverFunc, NULL);
    if (threadOpsCode) {
        perror("Klient: blad przy tworzeniu watku odbiorcy");
        exit(EXIT_FAILURE);
    }

    threadOpsCode = pthread_join(threadSender, NULL);
    if (threadOpsCode) {
        perror("Klient: blad przy przylaczaniu watku nadawcy");
        exit(EXIT_FAILURE);
    }

    threadOpsCode = pthread_join(threadReceiver, NULL);
    if (threadOpsCode) {
        perror("Klient: blad przy przylaczaniu watku odbiorcy");
        exit(EXIT_FAILURE);
    }

    printf("Klient sie skonczyl\n");
    exit(EXIT_SUCCESS);
}


void* senderFunc(void* _args) {
    msgQueueCommunication communicator;
    msgQueueProcType communicationProcess;

    //pobierz dane o kolejce do queueInfo
    struct msqid_ds queueInfo;
    msgQueueGetInfo(queueID, &queueInfo, 0);

    //maksyymalna ilosc wiadomosci w kolejce
    //pomniejszone o 1 aby bylo miejsce na odpowiedz serwera
    unsigned long maxMsgInQueue = queueInfo.msg_qbytes / MSGQUEUE_MSG_SIZE - 1;
    int msgSendReturn;

    printf("Klient: max komnunikatow w kolejce: %ld\n", maxMsgInQueue);

    while (1) {
//        if (flagReceived) {
            communicator.receiver = communicationProcess = SERVER;
            communicator.sender = (long int)getpid();

            printf("Podaj tresc komunikatu: ");
            scanf("%s", &(communicator.mtext[0]));
            if (communicator.mtext[0] == EOF) break;

            printf("Klient: Rozpoczynam wysylanie... %s -> %ld \n", communicator.mtext, communicator.receiver);

            //pobierz aktualne informacje o kolejce (zeby miec ilosc wiadomosci w kolejce aktualnie)
            msgQueueGetInfo(queueID, &queueInfo, 0);
            if (queueInfo.msg_qnum >= maxMsgInQueue) {
                printf("Klient: czekam na miejsce w kolejce...\n");
                while (queueInfo.msg_qnum >= maxMsgInQueue) {
                    msgQueueGetInfo(queueID, &queueInfo, 0);
                    //sleep(1);
                }
            }

            //wysylanie wiadomosci, w razie przerwania "zatrzymaj" program
            do {
                msgSendReturn = msgsnd(queueID, &communicator, MSGQUEUE_MSG_SIZE, IPC_NOWAIT);
            } while (msgSendReturn == -1 && errno == EINTR);

            //blad inny niz przerwanie
            if(msgSendReturn == -1) {
                perror("Klient: sender msgsnd error\n");
                exit(EXIT_FAILURE);
            }

            printf("Klient: WYSLANO %s -> %ld\n", communicator.mtext, communicator.receiver);
            flagReceived = 0;
//        }
    }

    pthread_exit(0);
}


void* receiverFunc(void* _args) {
    msgQueueCommunication communicator;
    communicator.receiver = (long int)getpid();
    int msgReceiveReturn;

    while(1) {
        do {
            msgReceiveReturn = msgrcv(queueID, &communicator, MSGQUEUE_MSG_SIZE, communicator.receiver, MSG_NOERROR);
        } while (msgReceiveReturn == -1 && errno == EINTR);

        if (msgReceiveReturn == -1) {
            perror("Klient: receiver msgrcv error");
            if (errno == EIDRM) printf("Zamknieto serwer!\n");
            exit(EXIT_FAILURE);
        }

        printf("\n\t----> odebrano: \"%s\"\n", communicator.mtext);
        flagReceived = 1;
    }

    pthread_exit(0);
}
